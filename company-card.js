import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-image/iron-image.js';

/**
 * `company-card`
 * Company card
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class CompanyCard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          --iron-icon-width: 16px;
          --iron-icon-height: 16px;
          --iron-icon-fill-color: var(--paper-grey-500);
        }
        
        .horizontal {
          @apply --layout-horizontal;
        }
        
        .vertical {
          @apply --layout-vertical;
        }
        
        .justified {
          @apply --layout-justified;
        }
        
        .center {
          @apply --layout-center-justified;
        }
        
        .end {
          @apply --layout-end-justified;
        }
        
        .flex {
          @apply --layout-flex;
        }
        
        .card {
          margin: 16px;
          color: var(--paper-grey-900);
          border-radius: 5px;
          background-color: var(--paper-grey-50);
          @apply --shadow-elevation-12dp;
        }
        
        .company-image {
          width: 220px;
          height: 220px;
        }
        
        .content-info {
          height: 200px;
          width: 290px;
          min-height: 200px;
          min-width: 290px;
          max-height: 200px;
          padding: 10px;
        }
        
        .cover-image {
          width: 100%;
          height: 100%;
        }
        
        .service-image {
          width: 50px;
          height: 50px;
          padding: 2px;
        }
        
        .button {
          background-color: var(--paper-indigo-500);
          color: var(--paper-grey-50);
        }
        
        .title {
          @apply --paper-font-title;
          color: var(--paper-grey-900);
        }
        
        .summary {
          @apply --paper-font-caption;
          color: var(--paper-grey-500);
        }
        
        .padding-summary {
          padding-top: 5%;
          padding-bottom: 2%;
          padding-right: 10%;
          padding-left: 10%;
        }
        
        .margin-button {
          margin-top: 5%;
        }
        
      </style>
      
      <!-- Company card -->
      <div class="horizontal card">
      
        <!-- Image -->
        <div class="company-image">
           <img class="cover-image" src="[[cover]]"></img>
        </div>
        <!-- Content info -->
        <div class="vertical content-info">
        
          <!-- Title -->
          <div class="horizontal justified">
            <span class="title">[[name]]</span>
            
            <div class="vertical center">
            
              <!-- Rating -->
              <div class="horizontal center">
                <iron-icon icon="icons:star"></iron-icon>
                <iron-icon icon="icons:star"></iron-icon>
                <iron-icon icon="icons:star"></iron-icon>
                <iron-icon icon="icons:star"></iron-icon>
                <iron-icon icon="icons:star"></iron-icon>
              </div>
              
            </div>
          </div>
          
          <!-- Icons -->
          <div class="horizontal justified padding-summary">
            
            <!-- time -->
            <div class="vertical">
              <div class="horizontal center">
                <iron-icon icon="icons:query-builder"></iron-icon>
              </div>
              <div class="horizontal center">
                <span class="summary">[[time]]</span>
              </div>
            </div>
            
            <!-- location -->
            <div class="vertical">
              <div class="horizontal center">
                <iron-icon icon="icons:room"></iron-icon>
              </div>
              <div class="horizontal center">
                <span class="summary">[[location]]</span>
              </div>
            </div>
            
          </div>
          
          <!-- service images -->
          <div class="horizontal justified">
            <template is="dom-repeat" items="[[services]]">
              <div class="service-image">
                <iron-image class="cover-image" sizing="contain" src="[[item.cover]]"></iron-image>
              </div>
            </template>
          </div>
          
          <!-- see more -->
          <div class="horizontal end">
            <a href="[[href]]">  
              <paper-button raised class="button margin-button">see more</paper-button>
            </a>
          </div>
          
        </div>
        
      </div>
    `;
  }
  
  static get properties() {
    return {
      cover: {
        type: String,
        value: 'https://i.imgur.com/Zjkxuai.png'
      },
      name: {
        type: String,
        value: 'Company name'
      },
      rating: {
        type: Number,
        value: 0.0
      },
      services: {
        type: Array,
        value: [
          {"cover": "https://i.imgur.com/Zjkxuai.png"},
          {"cover": "https://i.imgur.com/Zjkxuai.png"},
          {"cover": "https://i.imgur.com/Zjkxuai.png"},
          {"cover": "https://i.imgur.com/Zjkxuai.png"},
          {"cover": "https://i.imgur.com/Zjkxuai.png"}
        ]
      },
      time: {
        type: String,
        value: 'Not available'
      },
      location: {
        type: String,
        value: 'Not available'
      },
      href: {
        type: String,
        value: '#'
      }
    };
  }
}

window.customElements.define('company-card', CompanyCard);
